var m = require('mithril');
var DND = require('./../src/mdnd.js');

var State = {
    list:  [
        {id: 0, sortIndex: 0, name: 'Australia'},
        {id: 1, sortIndex: 1, name: 'Belgium'},
        {id: 2, sortIndex: 2, name: 'Canada'},
        {id: 3, sortIndex: 3, name: 'Denmark'},
        {id: 4, sortIndex: 4, name: 'Egypt'},
        {id: 5, sortIndex: 5, name: 'Finland'},
        {id: 6, sortIndex: 6, name: 'Germany'}
    ],
    list2: [
        {id: 0, sortIndex: 0, name: 'January'},
        {id: 1, sortIndex: 1, name: 'February'},
        {id: 2, sortIndex: 2, name: 'March'},
        {id: 3, sortIndex: 3, name: 'April'},
        {id: 4, sortIndex: 4, name: 'May'},
        {id: 5, sortIndex: 5, name: 'June'},
        {id: 6, sortIndex: 6, name: 'July'},
        {id: 7, sortIndex: 7, name: 'August'},
        {id: 8, sortIndex: 8, name: 'September'},
        {id: 9, sortIndex: 9, name: 'October'},
        {id: 10, sortIndex: 10, name: 'November'},
        {id: 11, sortIndex: 11, name: 'December'}
    ],
    list3:  [
        {id: 0, sortIndex: 0, name: 'alligator'},
        {id: 1, sortIndex: 1, name: 'bat'},
        {id: 2, sortIndex: 2, name: 'cat'},
        {id: 3, sortIndex: 3, name: 'dog'},
        {id: 4, sortIndex: 4, name: 'elephant'}
    ],
    list4: [
        {id: 5, sortIndex: 0, name: 'fish'},
        {id: 6, sortIndex: 1, name: 'gecko'},
        {id: 7, sortIndex: 2, name: 'hippo'},
        {id: 8, sortIndex: 3, name: 'ibex'},
        {id: 9, sortIndex: 4, name: 'jackal'}
    ],
    list7: [
        {id: 0, sortIndex: 0, name: 'Abbott, Edwin Abbott', content: 'Flatland'},
        {id: 1, sortIndex: 1, name: 'Burger, Dionys', content: 'Sphereland'},
        {id: 2, sortIndex: 2, name: 'Crane, Stephen', content: 'The Open Boat and Other Stories'},
        {id: 3, sortIndex: 3, name: 'Darwin, Charles', content: 'On the Origin of Species'},
        {id: 4, sortIndex: 4, name: 'Emerson, Ralph Waldo', content: 'Nature'}
    ]
};

State.list5 = State.list3.slice();
State.list6 = State.list4.slice();
State.list8 = State.list.slice();

var listenerOutput = '';

var Examples = {
    view: function() {
        return m('#examples', [
            m('h1', 'mithril-dnd Examples'),
            m('p', 'All the examples will require the following setup. Require mithril-dnd:'),
            m('pre', 'var DND = require(\'mithril-dnd\');'),
            m('p', 'And include a mirror somewhere near the bottom of the page. You only need one for every DND list on the page.'),
            m('pre', 'm(DND.mirror)'),
            m('h2', 'Simple Example'),
            m('p', 'Contains only the necessary parameters to use it.'),
            m('.example1', [
                m(DND.view, { list: State.list, template: function(item) {
                    return m('span.title', item.name);
                }})
            ]),
            m('pre', 'm(DND.view, {\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn m(\'span.title\', item.name);\n\t}\n})'),
            m('h2', 'Add Selectors'),
            m('p', 'Add classes or change the DOM elements.'),
            m('.example2', [
                m(DND.view, { selector: 'ul', itemSelector: 'li.green', list: State.list2, template: function(item) {
                    return m('span.title', item.name);
                }})
            ]),
            m('pre', 'm(DND.view, {\n\tselector: \'ul\',\n\titemSelector: \'li.green\',\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn m(\'span.title\', item.name);\n\t}\n})'),
            m('h2', 'Group Lists'),
            m('p', 'Lists can be grouped so that dragging and dropping only occurs in specific groups of lists. Also useful if you expect your data to change.'),
            m('.example3', [
                m(DND.view, { groupid: 'example3-A', list: State.list3, template: function(item) {
                    return m('span.title', item.name);
                }}),
                m(DND.view, { groupid: 'example3-A', list: State.list4, template: function(item) {
                    return m('span.title', item.name);
                }}),
                m(DND.view, { groupid: 'example3-B', list: State.list5, template: function(item) {
                    return m('span.title', item.name);
                }}),
                m(DND.view, { groupid: 'example3-B', list: State.list6, template: function(item) {
                    return m('span.title', item.name);
                }})
            ]),
            m('pre', 'm(DND.view, {\n\tgroupid: \'example3-A\',\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn m(\'span.title\', item.name);\n\t}\n}), (DND.view, {\n\tgroupid: \'example3-B\',\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn m(\'span.title\', item.name);\n\t}\n})'),
            m('h2', 'Filter Draggable Regions'),
            m('p', 'Limit the draggable region on an item in the list. Useful for making draggable headers, while allowing for other kinds of interaction on the rest of the item.'),
            m('.example4', [
                m(DND.view, { list: State.list7, draggable: function(e) {
                    return e.target.classList.contains('title') ? e.target : null;
                }, template: function(item) {
                    return [m('span.title', item.name), m('.content', item.content)];
                }})
            ]),
            m('pre', 'm(DND.view, {\n\tdraggable: function(e) {\n\t\treturn e.target.classList.contains(\'title\') ? e.target : null;\n\t},\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn [m(\'span.title\', item.name), m(\'.content\', item.content)];\n\t}\n})'),
            m('h2', 'Event Listeners'),
            m('p', 'You can listen to drag and drop events.'),
            m('.example5', [
                m(DND.view, { list: State.list8, onDrag: function(e, data) {
                    var isTouch = e.touches && e.touches.length > 0;
                    var x = isTouch ? e.touches[0].clientX : e.clientX;
                    var y = isTouch ? e.touches[0].clientY : e.clientY;
                    listenerOutput = 'Drag event fired\n' + JSON.stringify(data) + '\n(' + x + 'px, ' + y + 'px)';
                }, onDragging: function(e, data) {
                    var isTouch = e.touches && e.touches.length > 0;
                    var x = isTouch ? e.touches[0].clientX : e.clientX;
                    var y = isTouch ? e.touches[0].clientY : e.clientY;
                    listenerOutput = 'Dragging event fired\n' + JSON.stringify(data) + '\n(' + x + 'px, ' + y + 'px)';
                }, onDrop: function(e, data) {
                    var isTouch = e.touches && e.touches.length > 0;
                    var x = isTouch ? e.touches[0].clientX : e.clientX;
                    var y = isTouch ? e.touches[0].clientY : e.clientY;
                    listenerOutput = 'Drop event fired\n' + JSON.stringify(data) + '\n(' + x + 'px, ' + y + 'px)';
                }, template: function(item) {
                    return m('span.title', item.name);
                }}),
                m('pre', listenerOutput)
            ]),
            m('pre', 'm(DND.view, {\n\tonDrag: function(e, data) {\n\t\tconsole.log(\'Drag event fired\', data, \'(\' + e.x + \'px, \' + e.y + \')\');\n\t},\n\tonDragging: function(e, data) {\n\t\tconsole.log(\'Dragging event fired\', data, \'(\' + e.x + \'px, \' + e.y + \')\');\n\t},\n\tonDrop: function(e, data) {\n\t\tconsole.log(\'Drop event fired\', data, \'(\' + e.x + \'px, \' + e.y + \')\');\n\t},\n\tlist: [array of items],\n\ttemplate: function(item) {\n\t\treturn [m(\'span.title\', item.name), m(\'.content\', item.content)];\n\t}\n})'),
            m(DND.mirror)
        ]);
    }
};

m.mount(document.body, Examples)
