var m = require('mithril');
var DND = require('./model.js');
var Mirror = require('./mirror.js');
var TouchOver = require('./touchover.js');

module.exports = {
    view: function() {
        var groupMatches = function(dndid) {
            return dndid !== null && DND.groupid === dndid;
        };
        var dndClass = function(item, dndid) {
            return groupMatches(dndid) && DND.drag && item === DND.drag.item ? 'dragging' : groupMatches(dndid) && DND.drop && item === DND.drop.item ? 'dropping' : ''
        };

        var groupid;
        var touchoverFn;

        var generateData = function(vnode) {
            groupid = vnode.attrs.groupid ? vnode.attrs.groupid : 'DND-NONAME-' + DND.unnamedIndex++;
            DND.data[groupid] = DND.data[groupid] || [];
            let groupdata = DND.data[groupid];
            let columnIndex = groupdata.indexOf(vnode.attrs.list);
            if (columnIndex < 0) {
                groupdata.push(vnode.attrs.list);
            }
        }

        return {
            oninit: function(vnode) {
                generateData(vnode);
            },
            view: function(vnode) {
                //We use group id to determine if data changes
                groupid = vnode.attrs.groupid ? vnode.attrs.groupid : groupid;
                if (!DND.data.hasOwnProperty(groupid)) {
                    generateData(vnode);
                }
                let columnIndex = DND.data[groupid].indexOf(vnode.attrs.list);
                if (columnIndex < 0) {
                    let groupdata = DND.data[groupid];
                    groupdata.push(vnode.attrs.list);
                    DND.data[groupid] = groupdata;
                    columnIndex = groupdata.length - 1;
                }
                return m((vnode.attrs.selector ? vnode.attrs.selector : '') + '.dnd-list', {
                    style: 'min-height: ' + (groupMatches(groupid) && DND.drag && columnIndex === DND.drag.index ? DND.minHeight : 'none') + ';',
                    onmouseover: function(e) {
                        if (e.currentTarget === e.target && DND.drag && groupMatches(groupid)) {
                            DND.drop = {item: -1, index: columnIndex, column: e.currentTarget.closest('.dnd-list')};
                        }
                    }
                }, vnode.attrs.list.map(function(item, itemIndex) {
                    let itemRender = m(TouchOver, {
                        selector: (vnode.attrs.itemSelector ? vnode.attrs.itemSelector : '') + '.drag-item',
                        touchover: function(e) {
                            if (groupMatches(groupid) && item !== DND.drag.item) {
                                DND.drop = {item: item, index: columnIndex, column: vnode.dom};
                            }
                        },
                        class: dndClass(item, groupid),
                        index: itemIndex,
                        onmousedown: function(e) {
                            DND.handleDragStart(e, vnode.attrs, {groupid: groupid, item: item, columnIndex: columnIndex, itemIndex: itemIndex});
                        },
                        onmouseover: function(e) {
                            if (DND.drag && groupMatches(groupid)) {
                                DND.drop = {item: item, index: columnIndex, column: e.currentTarget.closest('.dnd-list')};
                            }
                        },
                        onmouseout: function(e) {
                            if (DND.drag && (!e.relatedTarget || (e.relatedTarget && !e.relatedTarget.closest('.mdnd--shadow')))) {
                                DND.drop = null;
                            }
                        },
                        ontouchstart: function(e) {
                            DND.handleDragStart(e, vnode.attrs, {groupid: groupid, item: item, columnIndex: columnIndex, itemIndex: itemIndex});
                        }
                    },
                        vnode.attrs.template(item, itemIndex)
                    );

                    let shadow = m((vnode.attrs.itemSelector ? vnode.attrs.itemSelector : ''), {class: 'mdnd--shadow'}, DND.drag ? vnode.attrs.template(DND.drag.item) : '');
                    let last = DND.drop && DND.drop.item === -1 && itemIndex === vnode.attrs.list.length - 1 && DND.drop.index === columnIndex && groupMatches(groupid);
                    let hasShadow = DND.drop && item === DND.drop.item && groupMatches(groupid);
                    let key = vnode.attrs.itemKey ? vnode.attrs.itemKey(item) : item.id;
                    let fragmentOptions = key !== null ? {key: key} : {};
                    return last ? m.fragment(fragmentOptions, [itemRender, shadow]) : hasShadow ? m.fragment(fragmentOptions, [shadow, itemRender]) : m.fragment(fragmentOptions, [itemRender]);
                }));
            },
            oncreate: function(vnode) {
                let columnIndex = DND.data[groupid].indexOf(vnode.attrs.list);
                touchoverFn = function(e) {
                    var lastIndex = vnode.attrs.list.length - (vnode.attrs.list.indexOf(DND.drag.item) === vnode.attrs.list.length - 1 ? 2 : 1);
                    var lastitem = e.currentTarget.querySelectorAll('.drag-item')[lastIndex];
                    if ((lastitem || lastIndex < 0) && groupMatches(groupid) && e.currentTarget === e.target) {
                        var contentY = lastIndex < 0 ? 0 : lastitem.offsetTop + lastitem.getBoundingClientRect().height;
                        if (contentY < e.detail.pageY) {
                            DND.drop = {item: -1, index: columnIndex, column: vnode.dom};
                        }
                    }
                };
                vnode.dom.addEventListener('touchover', touchoverFn);
            },
            onremove: function(vnode) {
                vnode.dom.removeEventListener('touchover', touchoverFn);
            }
        };
    },
    mirror: Mirror,
    destroy: DND.destroy
};
