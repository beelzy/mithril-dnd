var m = require('mithril');
var DND = {
    drag: null,
    drop: null,
    dragFn: null,
    moveFn: null,
    dropFn: null,
    groupid: null,
    template: null,
    itemSelector: '',
    unnamedIndex: 0,
    minHeight: 'none',
    mirror: {
        element: "",
        x: 0,
        y: 0,
        offset: [0, 0],
        width: 0,
        height: 0
    },
    data: {},
    destroy: function() {
        DND.data = {};
        DND.drag = null;
        DND.drop = null;
        DND.dragFn = null;
        DND.moveFn = null;
        DND.dropFn = null;
        DND.groupid = null;
        DND.template = null;
        DND.itemSelector = '';
        DND.unnamedIndex = 0;
        DND.minHeight = 'none';
    },
    getIndexes: function() {
        var draggedIdx = DND.data[DND.groupid][DND.drag.index].indexOf(DND.drag.item);
        var droppedIdx = DND.drop ? DND.drop.item === -1 ? DND.data[DND.groupid][DND.drop.index].length : DND.data[DND.groupid][DND.drop.index].indexOf(DND.drop.item) : null;

        var insertionIdx = droppedIdx !== null && draggedIdx < droppedIdx && DND.drag.index === DND.drop.index && DND.drop.item === -1 ? droppedIdx + 1 : droppedIdx;
        var deletionIdx = droppedIdx !== null && draggedIdx > droppedIdx && DND.drag.index === DND.drop.index ? draggedIdx + 1 : draggedIdx;
        return {insert: insertionIdx, delete: deletionIdx};
    },
    handleDragStart: function(e, attrs, options) {
        var isTouch = e.touches && e.touches.length > 0;
        var x = isTouch ? e.touches[0].clientX : e.clientX;
        var y = isTouch ? e.touches[0].clientY : e.clientY;
        if (!attrs.draggable || (attrs.draggable && attrs.draggable(e))) {
            e.preventDefault();
            let draggable = attrs.draggable ? attrs.draggable(e) : e.target;
            let rect = draggable.getBoundingClientRect();
            let styles = window.getComputedStyle(e.currentTarget);
            let containerStyles = window.getComputedStyle(e.currentTarget.closest('.dnd-list'));
            DND.itemSelector = attrs.itemSelector ? attrs.itemSelector : '';
            DND.minHeight = containerStyles.getPropertyValue('height');
            DND.groupid = options.groupid;
            DND.moveFn = attrs.onDragging;
            DND.dropFn = attrs.onDrop;
            DND.mirror.width = styles.getPropertyValue('width');
            DND.mirror.height = styles.getPropertyValue('height');
            DND.mirror.offset = [x - rect.left, y - rect.top];
            DND.drag = {item: options.item, index: options.columnIndex, column: e.currentTarget.closest('.dnd-list'), target: e.currentTarget};
            DND.mirror.element = e.currentTarget.outerHTML;
            DND.mirror.x = x - DND.mirror.offset[0];
            DND.mirror.y = y - DND.mirror.offset[1];
            DND.template = attrs.template;
            if (attrs.onDrag) {
                attrs.onDrag(e, {element: {
                    target: DND.drag.target,
                    from: DND.drag.column
                }, data: {
                    target: DND.drag.item,
                    from: {index: options.itemIndex, columnIndex: options.columnIndex}
                }
                });
            }
            if (isTouch) {
                window.addEventListener('touchmove', DND.handleDragging);
                window.addEventListener('touchend', DND.handleDrop);
            } else {
                window.addEventListener('mousemove', DND.handleDragging);
                window.addEventListener('mouseup', DND.handleDrop);
            }
        }
    },
    handleDragging: function(e) {
        var isTouch = e.touches && e.touches.length > 0;
        DND.mirror.x = (isTouch ? e.touches[0].clientX : e.clientX) - DND.mirror.offset[0];
        DND.mirror.y = (isTouch ? e.touches[0].clientY : e.clientY) - DND.mirror.offset[1];

        if (isTouch && DND.drag) {
            var overElements = document.elementsFromPoint(e.touches[0].clientX, e.touches[0].clientY);
            var overElement = overElements[0];
            for (var i = 0, size = overElements.length; i < size; i++) {
                overElement = overElements[i];
                if (!overElement.classList.contains('mdnd--shadow') && (overElement.classList.contains('drag-item') || overElement.classList.contains('dnd-list'))) {
                    overElement.dispatchEvent(new CustomEvent('touchover', {detail: e.touches[0]}));
                    if (!overElement.closest('.dnd-list')) {
                        DND.drop = null;
                    }
                    break;
                }
            }
        }

        if (DND.moveFn) {
            var indexes = DND.getIndexes();
            DND.moveFn(e, {element: {
                target: DND.drag.target,
                to: DND.drop ? DND.drop.column : null,
                from: DND.drag.column
            }, data: {
                target: DND.drag.item,
                to: {index: indexes.insert, columnIndex: DND.drop ? DND.drop.index : null},
                from: {index: indexes.delete, columnIndex: DND.drag.index}
            }
            });
        }
        m.redraw();
    },
    handleDrop: function(e) {
        if (e.touches) {
            e.clientX = DND.mirror.x + DND.mirror.offset[0];
            e.clientY = DND.mirror.y + DND.mirror.offset[1];
        }
        if (DND.drag && DND.drop) {
            var indexes = DND.getIndexes();

            if (!(indexes.insert === indexes.delete && DND.drag.index === DND.drop.index)) {
                DND.data[DND.groupid][DND.drop.index].splice(indexes.insert, 0, DND.drag.item);
                DND.data[DND.groupid][DND.drag.index].splice(indexes.delete, 1);
            }
            if (DND.dropFn) {
                DND.dropFn(e, {element: {
                    target: DND.drag.target,
                    to: DND.drop.column,
                    from: DND.drag.column
                }, data: {
                    target: DND.drag.item,
                    to: {index: indexes.insert, columnIndex: DND.drop.index},
                    from: {index: indexes.delete, columnIndex: DND.drag.index}
                }
                });
            }
            DND.dropFn = null;
        }
        DND.moveFn = null;
        DND.minHeight = 'none';
        DND.drag = null;
        DND.drop = null;
        DND.mirror.element = "";
        if (e.touches) {
            window.removeEventListener('touchmove', DND.handleDragging);
            window.removeEventListener('touchend', DND.handleDrop);
        } else {
            window.removeEventListener('mousemove', DND.handleDragging);
            window.removeEventListener('mouseup', DND.handleDrop);
        }
        m.redraw();
    }
};

module.exports = DND;
