var m = require('mithril');

module.exports = function() {
    var touchoverFn;
    return {
        oncreate: function(vnode) {
            touchoverFn = vnode.attrs.touchover;
            vnode.dom.addEventListener('touchover', touchoverFn);
        },
        onremove: function(vnode) {
            vnode.dom.removeEventListener('touchover', touchoverFn);
        },
        view: function(vnode) {
            return m(vnode.attrs.selector ? vnode.attrs.selector : '', vnode.attrs, vnode.children);
        }
    };
};
