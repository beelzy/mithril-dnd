var m = require('mithril');
var DND = require('./model.js');

module.exports = {
    view: function(vnode) {
        return DND.drag ? m((vnode.attrs.selector ? vnode.attrs.selector : '') + '.mdnd--mirror', {style: 'transform: translate(' + DND.mirror.x + 'px, ' + DND.mirror.y + 'px); width: ' + DND.mirror.width + '; height: ' + DND.mirror.height + ';'},
            m(DND.itemSelector + '.drag-item', DND.template(DND.drag.item))) : '';
    }
};
