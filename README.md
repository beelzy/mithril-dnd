# Simple Mithril Drag and Drop

Should work with Mithril v1 and 2.

## [Demo](https://beelzy.gitlab.io/mithril-dnd/)

## Usage

Make a DND list where drag and drop should occur:

```
var DND = require('mithril-dnd');

m(DND.view, {list: [your array data here], template: function(data) {
        return m('.item', data.name);
        }
        }
 );
```

Include the mirror div somewhere in your page (preferably at the bottom):

```
m(DND.mirror);
```

Also include `mdnd.css`.

## Event Listeners

You can listen to events for drag start, dragging and dropping. The listener will return back the event and a data object with the following format:

```
{element: {
target: (dragged DOM element),
            from: (dragged list DOM element),
            to: (dropped list DOM element, if there is one)
          }, data: {
target: (dragged item from the list),
            from: {index: (dragged index in the list), columnIndex: (dragged list index)},
            to: {index: (dropped index in the list), columnIndex: (dropped list index)}
          }
}
```

All the listeners will provide data for the dragged items, but `onDragging` and `onDrag` may not have a dropped item.

## Options

You can provide other attributes to a DND list for greater configurability:

| **Option** | **Required** | **Type** | **Explanation** |
| --- | --- | ------- | ------- |
| list | yes | array | Contains the data you want to be draggable. |
| template | yes | function(item) | Returns a Mithril renderer for each item in the list. `item` is an item in the array specified in the `list` option. |
| groupid | no | string | Id for grouping multiple DND lists. You can drag and drop items between lists in the same group. |
| draggable | no | function(e) | A function that filters which parts of a draggable item should be draggable or not. If the element dragged should allow the item to be dragged, return the entire element you want to be draggable or null if the dragged element is not allowed to initiate dragging. `e` is a vanilla javascript event sent by the dragging action. |
| selector | no | string | Optional selector for the container element of the list. You can use this to add additional classes or specify the tags you want to use for the container element. Can also be used on the mirror element. |
| itemSelector | no | string | Optional selector for each item element in the list. You can use this to add additional classes or specify the tags you want to use for each item element. |
| itemKey | no | function(item) | If your list needs to be keyed, use this function to return the key value. `item` is an item in the array specified in the `list` option. |
| onDrag | no | function(e, {data object}) | Listener that gets fired when dragging starts. |
| onDragging | no | function(e, {data object}) | Listener that gets fired during dragging. |
| onDrop | no | function(e, {data object}) | Listener that gets fired during drop. |
